const moment = require("moment-timezone");
const axios = require("axios");

const SAO_PAULO_TIMEZONE = "America/Sao_Paulo";

const FILE_PREFIX = "https://dasa-twilio-functions-8539-prd.twil.io";

exports.handler = async function (context, event, callback) {
  class StatusFeriado {
    static ABERTO = new StatusFeriado("aberto");

    static FECHADO = new StatusFeriado("fechado");

    constructor(name) {
      this.name = name;
    }
  }

  class DiaSemanaGrupo {
    static DIA_UTIL = new DiaSemanaGrupo("diaUtil");

    static SABADO = new DiaSemanaGrupo("sabado");

    static DOMINGO = new DiaSemanaGrupo("domingo");

    constructor(name) {
      this.name = name;
    }
  }

  class DataTrabalho {
    constructor(datasFeriados, regraHorarioAtendimento, mockBusinessHour, mockHour, mockTmz, { tmz = SAO_PAULO_TIMEZONE }) {
      this.datasFeriados = datasFeriados;
      this.regraHorarioAtendimento = regraHorarioAtendimento;
      this.mockBusinessHour = mockBusinessHour;
      this.tmz = tmz;
      this.mockHour = mockHour;
      this.mockTmz = mockTmz;

      this.mockValido = false;
      this.dataComFuso = this.definirData();
      this.diaAtual = this.dataComFuso.date();
      this.mesAtual = this.dataComFuso.month();
      this.anoAtual = this.dataComFuso.year();
      this.diaDaSemana = this.dataComFuso.weekday();
      this.horaAtual = this.dataComFuso.hour();
      this.minutosAtual = this.dataComFuso.minute();
      this.feriado = this.obterFeriado();
      this.tipoDia = this.obterTipoDia();

      this.regraHorarioAtendimentoPorTipoDia = this.regraHorarioAtendimento[this.tipoDia];
      this.dataHoraInicioAtendimento = this.obterDataHora("inicio");
      this.dataHoraFimAtendimento = this.obterDataHora("fim");
      this.dataHoraInicioAtendimentoFeriado = this.obterDataHoraFeriado("inicio");
      this.dataHoraFimAtendimentoFeriado = this.obterDataHoraFeriado("fim");
    }

    definirData() {
      const formatoValidacao = this.mockTmz ? "YYYY-MM-DD HH:mm Z" : "YYYY-MM-DD HH:mm";
      const horarioComFuso = this.mockTmz ? `${this.mockHour} -${this.mockTmz}` : this.mockHour;
      const mockValido = horarioComFuso && moment(horarioComFuso, formatoValidacao, true).isValid();

      this.mockValido = mockValido;
      const dataAtual = this.mockBusinessHour && mockValido ? new Date(horarioComFuso) : new Date();
      const fusoHorario =
        this.mockBusinessHour && mockValido ? moment(dataAtual.getTime()) : moment(dataAtual.getTime()).tz(this.tmz);

      return fusoHorario;
    }

    obterTipoDia() {
      return (
        (this.diaDaSemana % 6 !== 0 && DiaSemanaGrupo.DIA_UTIL.name) ||
        (this.diaDaSemana === 6 && DiaSemanaGrupo.SABADO.name) ||
        (this.diaDaSemana === 0 && DiaSemanaGrupo.DOMINGO.name)
      );
    }

    obterFeriado() {
      let feriado;
      const feriadosNoMes = this.datasFeriados.filter((hlds) => hlds.mes === this.mesAtual);
      if (feriadosNoMes.length > 0) {
        [feriado] = feriadosNoMes[0].datas.filter((data) => data.dia === this.diaAtual);
      }
      return feriado;
    }

    emHorarioDeAtendimento() {
      const horaAtual = new Date(Date.UTC(this.anoAtual, this.mesAtual, this.diaAtual, this.horaAtual, this.minutosAtual));
      if (this.feriado) {
        const feriadoStatus = this.feriado.status ? this.feriado.status.toLowerCase() : this.feriado.status;
        if (feriadoStatus === StatusFeriado.FECHADO.name) {
          return false;
        }
        if (this.dataHoraInicioAtendimentoFeriado && this.dataHoraFimAtendimentoFeriado) {
          if (horaAtual >= this.dataHoraInicioAtendimentoFeriado && horaAtual < this.dataHoraFimAtendimentoFeriado) {
            return true;
          }
        }
      }
      if ((this.dataHoraInicioAtendimento && this.dataHoraFimAtendimento) && (!this.dataHoraInicioAtendimentoFeriado && !this.dataHoraFimAtendimentoFeriado)) {
        if (horaAtual >= this.dataHoraInicioAtendimento && horaAtual < this.dataHoraFimAtendimento) {
          return true;
        }
      }
      return false;
    }

    obterDataHora(tipo) {
      let dataRetorno;
      if (this.regraHorarioAtendimentoPorTipoDia) {
        if (tipo === "inicio") {
          const inicioAtendimento = this.regraHorarioAtendimentoPorTipoDia.horarioInicio.split(":");
          const dataInicioAtendimento = new Date(
            Date.UTC(this.anoAtual, this.mesAtual, this.diaAtual, inicioAtendimento[0], inicioAtendimento[1])
          );
          dataRetorno = dataInicioAtendimento;
        } else {
          const fimAtendimento = this.regraHorarioAtendimentoPorTipoDia.horarioFim.split(":");
          const dataFimAtendimento = new Date(
            Date.UTC(this.anoAtual, this.mesAtual, this.diaAtual, fimAtendimento[0], fimAtendimento[1])
          );
          dataRetorno = dataFimAtendimento;
        }
      }
      return dataRetorno;
    }

    obterDataHoraFeriado(tipo) {
      let dataRetorno;
      if (this.feriado) {
        if (this.regraHorarioAtendimentoPorTipoDia && this.regraHorarioAtendimentoPorTipoDia.feriado) {
          if (tipo === "inicio") {
            const inicioAtendimento = this.regraHorarioAtendimentoPorTipoDia.feriado.horarioInicio.split(":");
            const dataInicioAtendimento = new Date(
              Date.UTC(this.anoAtual, this.mesAtual, this.diaAtual, inicioAtendimento[0], inicioAtendimento[1])
            );
            dataRetorno = dataInicioAtendimento;
          } else {
            const fimAtendimento = this.regraHorarioAtendimentoPorTipoDia.feriado.horarioFim.split(":");
            const dataFimAtendimento = new Date(
              Date.UTC(this.anoAtual, this.mesAtual, this.diaAtual, fimAtendimento[0], fimAtendimento[1])
            );
            dataRetorno = dataFimAtendimento;
          }
        }
      }
      return dataRetorno;
    }

    detalhes() {
      const dia = DataTrabalho.padronizaNumero(this.diaAtual);
      const mes = DataTrabalho.padronizaNumero(this.mesAtual + 1);
      const minutos = DataTrabalho.padronizaNumero(this.minutosAtual);
      return {
        dataHoraAtendimento: `HORARIO_ATENDIMENTO ${dia}/${mes} ${this.horaAtual}:${minutos}`,
        datasFeriados: JSON.stringify(this.datasFeriados),
        regraHorarioAtendimento: JSON.stringify(this.regraHorarioAtendimento),
        mockBusinessHour: this.mockBusinessHour,
        mockHour: this.mockHour,
        mockTmz: this.mockTmz,
        timezone: this.tmz,
        mockValido: this.mockValido,
        dataComFuso: this.dataComFuso.format("YYYY-DD-MM HH:mm:ss"),

        diaDaSemana: this.diaDaSemana,

        feriado: this.feriado,
        tipoDia: this.tipoDia,
        dataHoraInicioAtendimento: this.dataHoraInicioAtendimento,
        dataHoraFimAtendimento: this.dataHoraFimAtendimento,
      };
    }

    static padronizaNumero(numero) {
      return numero.toString().padStart(2, "0");
    }
  }

  const { nomeArquivoRegraHorario } = event;
  const regraHorario = await readFile(nomeArquivoRegraHorario);
  const { datasFeriados } = regraHorario;
  const { regraHorarioAtendimento } = regraHorario;

  if (datasFeriados || regraHorarioAtendimento) {
    const { tmz } = event;
    const mockBusinessHour = context.MOCK_BUSINESS_HOUR;
    const { mockHour } = event;
    const { mockTmz } = event;

    const dataTrabalho = new DataTrabalho(datasFeriados, regraHorarioAtendimento, mockBusinessHour, mockHour, mockTmz, {
      tmz,
    });
    const noHorarioDeAtendimento = dataTrabalho.emHorarioDeAtendimento();

    const objRetorno = {
      horario: noHorarioDeAtendimento,
      detalhes: dataTrabalho.detalhes(),
    };

    return callback(null, objRetorno);
  }
  const objRetorno = {
    horario: false,
    erro: "Informar data dos feriados e regra do horário de atendimento",
  };
  return callback(null, objRetorno);

  async function readFile(nomeArquivo) {
    const fileUrl = `${FILE_PREFIX}${nomeArquivo}`;
    const { data } = await axios.get(fileUrl);
    return data;
  }
};
