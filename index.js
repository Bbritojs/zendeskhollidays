require('dotenv').config();
const fetch = require('node-fetch');
const requestPromise = require('request-promise');
const userZendesk = process.env.USER_ZENDESK;
const tokenZendesk = process.env.TOKEN_ZENDESK;
const zendeskEndpoint = process.env.ZENDESK_ENDPOINT;
const fs = require('fs');

const dataCredentials = userZendesk + "/token:" + tokenZendesk //trocado a variavel email por user zendesk para que todos possam fazer uso dessa feature
const auth = new Buffer.from(dataCredentials).toString('base64')


function write() {
    let response;
    try {
        response = fs.readFileSync('./hollidays.json', "utf8")
        let responseParsed = JSON.parse(response)
        console.log("response",responseParsed.lamina);

    } catch (e) {
        response = e;
        console.log("Error", response.message)
    }
}

async function createTicket() {

    console.log(auth)
    console.log(userZendesk);
    console.log(zendeskEndpoint)
    const newticket = {
        "ticket": {
            "comment": {
                "body": "Testando uma nova abertura de ticket"
            },
            "subject": "Teste",
            "status": "open",
            "assignee_email": "bbrito@mundolivre.digital",
            "group_id": "360015428294",
            "custom_fields": [
                {
                    "id": 1500001248101, //responsavel
                    "value": "mld"
                }
            ]

        }
    }
    const link = {
        method: 'POST',
        uri: zendeskEndpoint + '/api/v2/tickets.json',
        params: {
            access: dataCredentials
        },
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + auth
        },
        json: newticket
    };
    try {
        const response = await requestPromise(link);

        updateTicket(JSON.stringify(response.ticket.id))
        console.log("response", typeof response.ticket.id)
    } catch (error) {
        console.log("deu erro irei mostrar o link", link)
        console.log('erro:', error)
    }

}

async function updateTicket(idTicket) {
    const update = {
        "ticket": {
            "comment": {
                "body": "Fechando o ticket test"
            },
            "status": "closed",
            "custom_fields": [
                {
                    "id": 1500000053761, //Produto
                    "value": "flex"
                },
                {
                    "id": 360056298473, //Prioridade
                    "value": "p2_-_parada_intermitente"
                },
                {
                    "id": 1500000765681, //Ticket Jira
                    "value": "N/A"
                },

                {
                    "id": 360056298074, //Solução
                    "value": "Solução teste"
                },

                {
                    "id": 1500001247901, //Categorização
                    "value": "usuário"
                },

            ]
        }
    }

    const link = {
        method: 'PUT',
        uri: zendeskEndpoint + '/api/v2/tickets/' + idTicket,
        params: {
            access: dataCredentials
        },
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + auth
        },
        json: update
    }

    try {

        const response = await requestPromise(link);
        console.log("vendo a resposta da atualização do ticket criado", response)

    } catch (e) {
        console.log("error", e.message)
    }


}

function app() {
    write();
    /* createTicket(); */

}
app();


